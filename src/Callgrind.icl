implementation module Callgrind

/**
 * Copyright 2019-2020, 2022 Camil Staps.
 *
 * This file is part of convertprofile.
 *
 * Convertprofile is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3 of the License.
 *
 * Convertprofile is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with convertprofile. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import PGCL

write_profile_to_callgrind :: !Profile !*File -> *File
write_profile_to_callgrind profile f
	# costs = compute_aggregate_costs profile
	# calls = compute_call_matrix profile
	= write_profile costs calls profile f

:: Call =
	{ call_count :: !Int
	, call_ticks :: !Int
	, call_words :: !Int
	}

compute_call_matrix :: !Profile -> .{#.{#Call}}
compute_call_matrix {cost_centres,profile}
	# calls =
		{ createArray
			(size cost_centres)
			{call_count=0,call_ticks=0,call_words=0}
		\\ _ <- [1..size cost_centres]
		}
	= aggregate_calls profile calls
where
	aggregate_calls :: !ProfileStack !*{#*{#Call}} -> *{#*{#Call}}
	aggregate_calls {cost_centre,children} calls
		# calls = foldr
			(\c calls
				# (call,calls) = calls![cost_centre,c.cost_centre]
				# call
					& call_count = call.call_count + c.scalls + c.lcalls + c.ccalls
					, call_ticks = call.call_ticks + c.cumulative_ticks
					, call_words = call.call_words + c.cumulative_words
				-> {calls & [cost_centre,c.cost_centre]=call})
			calls
			children
		# calls = foldr aggregate_calls calls children
		= calls

:: Costs =
	{ costs_ticks :: !Int
	, costs_words :: !Int
	}

compute_aggregate_costs :: !Profile -> .{#Costs}
compute_aggregate_costs {cost_centres,profile}
	# costs = createArray (size cost_centres) {costs_ticks=0,costs_words=0}
	= aggregate_costs profile costs
where
	aggregate_costs :: !ProfileStack !*{#Costs} -> *{#Costs}
	aggregate_costs p costs
		# (this,costs) = costs![p.cost_centre]
		# this
			& costs_ticks = this.costs_ticks + p.ticks
			, costs_words = this.costs_words + p.words
		# costs & [p.cost_centre] = this
		# costs = foldr aggregate_costs costs p.children
		= costs

write_profile :: !{#Costs} !{#{#Call}} !Profile !*File -> *File
write_profile costs calls p f
	# f = f <<< "# callgrind format\n"
	# f = f <<< "events: Cycles Words\n"
	# f = write_cost_centres (size p.cost_centres-1) costs calls p.cost_centres p.modules f
	= f

write_cost_centres :: !Int !{#Costs} !{#{#Call}} !{#CostCentre} !{#String} !*File -> *File
write_cost_centres -1 _ _ _ _ f = f
write_cost_centres i costs calls ccs mods f
	# f = f <<< "fl=" <<< mods.[ccs.[i].cc_module] <<< "\n"
	# f = f <<< "fn=" <<< ccs.[i].cc_name <<< "\n"
	# f = f <<< "0 " <<< costs.[i].costs_ticks <<< " " <<< costs.[i].costs_words <<< "\n"
	# f = write_calls (size ccs-1) calls.[i] f
	= write_cost_centres (i-1) costs calls ccs mods f
where
	write_calls -1 _ f = f 
	write_calls i calls f
		# f = case calls.[i] of
			{call_count=0}
				= f
			{call_count,call_ticks,call_words}
				# f = f <<< "cfi=" <<< mods.[ccs.[i].cc_module] <<< "\n"
				# f = f <<< "cfn=" <<< ccs.[i].cc_name <<< "\n"
				# f = f <<< "calls=" <<< call_count <<< " 0\n"
				# f = f <<< "0 " <<< call_ticks <<< " " <<< call_words <<< "\n"
				= f
		= write_calls (i-1) calls f
